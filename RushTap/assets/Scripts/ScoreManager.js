// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

//ユーザーデータの定義
var RushTapUserDate = {
    highScore: 0,
    adflg: null
};

cc.Class({
    extends: cc.Component,

    properties: {
        //ゲーム画面のハイスコアラベル
        gameHighScoreLabel: {
            default: null,
            type: cc.Label
        },

        //ゲーム画面のスコアラベル
        gameScoreLabel: {
            default: null,
            type: cc.Label
        },

        //リザルト画面のハイスコアラベル
        resultHighScore: {
            default: null,
            type: cc.Label
        },

        //リザルト画面のスコアラベル
        resultScore: {
            default: null,
            type: cc.Label
        },

        //セーブデータとして残すユーザーデータ
        _rushTapUserData: null,

        //スコア
        _score: 0
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        //セーブデータを読み込む
        this.readUserData();

        console.log(this._rushTapUserData.adflg);

        //起動時に1度だけハイスコアを更新する
        this.gameHighScoreLabel.string = 'High-Score: ' + this._rushTapUserData.highScore;
        this.resultHighScore.string = this._rushTapUserData.highScore;
    },

    update: function (dt) {
        //スコアの更新
        this.renewScoreLabel();

        //スコアがユーザーのデータに記録しているハイスコアを超えたら
        if (this._rushTapUserData.highScore < this._score) {
            this.renewHighScoreLabel();
        }
    },

    //スコアの加算処理
    addScore: function () {
        this._score = (this._score + 1)|0;
    },

    //ハイスコアの表示を更新
    renewHighScoreLabel: function () {
        this.gameHighScoreLabel.string = 'High-Score: ' + this._score;
        this.resultHighScore.string = this._score;
    },

    //スコアの表示を更新
    renewScoreLabel: function () {
        this.gameScoreLabel.string = 'Score: ' + this._score;
        this.resultScore.string = this._score;
    },

    //スコアを送る
    goScore: function () {
        return this._score;
    },

    //視聴フラグのセット(true)
    setWatchTrue: function () {
        this._rushTapUserData.adflg = true;
    },

    //視聴フラグのセット(false)
    setWatchFalse: function () {
        this._rushTapUserData.adflg = false;
    },

    //ゲームに視聴フラグを送る
    goWatchFlg: function () {
        return this._rushTapUserData.adflg;
    },

    //セーブデータへ書き込み
    writeUserData: function () {
        //スコアがユーザーのデータに記録しているハイスコアを超えたら
        if (this._rushTapUserData.highScore < this._score) {
            this._rushTapUserData.highScore = this._score;
        }
        //記録前にフラグを切り替える
        /*switch(this._rushTapUserData.adflg) {
            case true:
            this._rushTapUserData.adflg = false;
            break;

            case false:
            this._rushTapUserData.adflg = true;
            break;
        }*/
        console.log(this._rushTapUserData.adflg);
        //JSONオブジェクトを文字列にして保存
        cc.sys.localStorage.setItem('RushTapUserData', JSON.stringify(this._rushTapUserData));
    },

    //セーブデータから読み出し
    readUserData: function () {
        var data = cc.sys.localStorage.getItem('RushTapUserData');
        //確認用ログ
        cc.log(data);

        if (data !== null) {
            //見つかったらセーブファイルのデータを適用
            this._rushTapUserData = JSON.parse(data);
        }
        else {
            //見つからなかったら初期値をセット
            this._rushTapUserData = RushTapUserDate;
        }
    },
});
