// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

//変数宣言
//ScoreManagerを使用
var ScoreManager = require('ScoreManager');
//VPosChangeを使用
var VPosChange = require('VPosChange');

cc.Class({
    extends: cc.Component,

    properties: {
        //ScoreManager
        scoreManager: {
            default: null,
            type: ScoreManager
        },

        //VPosChange
        vPosChange: {
            default: null,
            type: VPosChange
        },
        
        //ゲームマネージャー
        gameManager: {
            default: null,
            type: cc.Node
        },

        //リザルトマネージャー
        resultManager: {
            default: null,
            type: cc.Node
        },

        //タイルプレファブ
        tilePrefab: {
            default: null,
            type: cc.Prefab
        },

        //スコアラベル
        scoreLabel: {
            default: null,
            type: cc.Label
        },

        //タッチスタートラベル
        gStartLabel: {
            default: null,
            type: cc.Node
        },

        //ゲームオーバーラベル
        gOverLabel: {
            default: null,
            type: cc.Node
        },

        //動画を見るか見ないかの確認画面
        videoScrrn: {
            default: null,
            type: cc.Node
        },

        //タイムゲージ
        timeGage: {
            default: null,
            type: cc.ProgressBar
        },

        //スタートボタン
        startButton: {
            default: null,
            type: cc.Node
        },

        //ボタンノード
        buttonNode: {
            default: null,
            type: cc.Node
        },

        //ゲームBGM
        gameBGM: {
            default: null,
            url: cc.AudioClip
        },

        //ボタンクリック時の効果音
        clickButtonAudio: {
            default: null,
            url: cc.AudioClip
        },

        //ゲームオーバー時に再生する効果音
        gOverAudio: {
            default: null,
            url: cc.AudioClip
        },

        //AudioID
        _audioID: 0,

        //ゲームスタートフラグ
        _isGameStart: false,

        //ゲームオーバーフラグ
        _isGameOver: false,

        //動画視聴フラグ1
        _isViewing: null,

        //動画視聴フラグ2
        _isViewing2: null,

        //乱数セット用変数
        _dice: null,

        //制限時間
        _time: 10,

        //ゲームオーバー時のちょっとした間
        _frizztime: 2,

        //現在のスコア
        _score: 0,

        //ステップアップする目標スコア
        _stepUpScore: 30,

        //ステップアップ
        _stepUp: 30,

        //一番最初に設置するタイルのx座標
        _startSetPosX: -300,

        //タイルを設置する横幅の間隔
        _setWidth: 200,

        //初期セットに使う配列
        activeNum0: [],

        //activeNum0更新用の配列
        activeNum1: [],

        //配列の要素を指すポインタ
        _arrayPointer: 0,

        //当たりの場所を格納
        _hitTile: 0,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        //音再生
        this._audioID = cc.audioEngine.play(this.gameBGM, true);

        //ScoreManagerから視聴フラグを受け取る
        this._isViewing = this.scoreManager.goWatchFlg();

        this._isViewing2 = false;

        this._dice = null;

        if (this._isViewing == null) {
            //抽選関数を呼び出して乱数をセット(0,1)のどれか1つ
            this._dice = this.random(0,1,1);
        }

        console.log(this._dice);

        //予め広告用の動画を読み込んでおく
        this.adPreload();

        //可変長配列の為配列の大きさを設定
        this.activeNum0.length = 4;
        this.activeNum1.length = 4;

        //タイルの生成
        //配列に数値をセット
        this.setArray(0);
        this.setArray(1);

        var verticaltile = 4;
        var sidetile = 4;

        //タイルの生成開始
        for (var vartical = 0; vartical < verticaltile; vartical = (vartical + 1)|0) {
            var active = this.activeNum0[vartical];
            for (var side = 0; side < sidetile; side = (side + 1)|0) {
                //タイル生成関数を呼び出す
                this.createTile(vartical, side, active);
            }
        }
        /*while (verticaltile < 4) {
            var active = this.activeNum0[verticaltile];
            while (sidetile < 4) {
                //タイル生成関数を呼び出す
                this.createTile(verticaltile, sidetile, active);
                sidetile = (sidetile + 1)|0;
            }
            verticaltile = (verticaltile + 1)|0;
        }*/

        //当たりの場所をボタンに送るのでセットする
        this._hitTile = this.activeNum0[this._arrayPointer];
    },

    update: function (dt) {
        //ゲーム画面に切り替わって画面をタップした
        if (this._isGameStart == true) {
            //ゲームオーバーじゃない間ゲージを常時減らす
            if (this._isGameOver != true) {
                //徐々にゲージを減らす
                this._time -= this._stepUp * dt;
                this.timeGage.progress = this._time / 10 * this._stepUp;
                //スコアが目標スコアに到達
                if (this._score >= this._stepUpScore) {
                    //ゲージの減少スピードを少しずつ上昇
                    this._stepUp += 0.1,
                    //ゲージ回復のボーダーを30ずつ上げる
                    this._stepUpScore += 30;
                    //制限時間を戻す
                    this._time = 10;
                }
            }
        }

        //制限時間が0になった
        if (this._time <= 0) {
            //ゲームオーバーフラグをtrueに
            this._isGameOver = true;
        }

        //ゲームオーバーフラグがtureになった
        if (this._isGameOver == true) {
            //音再生を止める。
            cc.audioEngine.stop(this._audioID);
            //ゲームオーバーラベルを表示する
            this.gOverLabel.active = true;
            //diceに0が入っていたら、または_isViewingがfalseなら
            if (this._dice == 0 || this._isViewing == false) {
                //視聴フラグをtrueに
                this.scoreManager.setWatchTrue();
            }
            this._frizztime -= dt;   
        }

        //_isViewingがfalse
        if (this._frizztime <= 0 && this._isViewing == false) {
            //確認画面を表示する
            this.videoScrrn.active = true;
        }
        //一度復帰してゲームオーバーになった
        else if (this._frizztime <= 0 && this._isViewing2 == true) {
            //リザルト画面に切り替える
            this.goResult();
        }
        //_isViewingがtrue
        else if (this._frizztime <= 0) {
            //視聴フラグをfalseに
            this.scoreManager.setWatchFalse();
            //リザルト画面に切り替える
            this.goResult();
        }
    },

    //タイル生成関数(varticalの番号, 何枚タイルを生成するか, 当たりのタイルの場所)
    createTile: function (node, num, active) {

        //tileプレファブを追加する場所を取得
        var tiles = cc.find('Canvas/gameManager/tiles/vartical' + node);
        //タイルプレファブを生成
        var vTiles = cc.instantiate(this.tilePrefab);
        //ノードを追加(子ノード, 描画優先度, タグ)
        tiles.addChild(vTiles, 0, node);

        //はずれのタイルを暗めに変更
        if (num != active) {
            vTiles.color = new cc.Color (128, 128, 128, 255);
        }
        //座標を指定してその場所へ設置する
        vTiles.setPosition(cc.p(this._startSetPosX + (this._setWidth * num), 0));
    },

    //抽選関数(最小値, 最大値, 回す回数)
    random: function (min, max, turn) {
        for (var i = 0 ; i < turn ; i = (i + 1)|0){
            //Math.random() 0.0~1.0までの乱数を生成
            //Math.floor 小数点を切り捨て
            //先に乱数を10倍してから切り捨てることで0~10までの整数を生成
            var num = Math.floor(Math.random () * 10);
            //console.log(num);
            //セットした最小値以下、または最大値以上の数値が出力されたらやり直し
            if (num < min || num > max) {
                i = (i - 1)|0;
            }
            //数値を返す
            else {
                //console.log('Go Array');
                return num;
            }
        }
    },

    //セット関数(0か1)
    setArray: function (num) {
        //numが0ならactiveNum0
        if (num == 0) {
            for (var i = 0; i < 4; i = (i + 1)|0) {
                //乱数生成関数で乱数を生成し配列にセット
                this.activeNum0[i] = this.random(0, 3, 4);
            }
            //console.log('activeNum0 ' + this.activeNum0);
        }
        //numが1ならactiveNum1
        else if (num == 1) {
            for (var i = 0; i < 4; i = (i + 1)|0) {
                //乱数生成関数で乱数を生成し配列にセット
                this.activeNum1[i] = this.random(0, 3, 4);
            }
            //console.log('activeNum1 ' + this.activeNum1);
        }
    },

    //スタートボタン
    isStartButton: function () {
        if (this._isGameStart == false)
        {
            this.startButton.active = false;
            this.gStartLabel.active = false;
            this._isGameStart = true;
        }
    },

    //ボタン0
    clickButton0: function () {
        if (this._isGameStart == true) {
            if (this._hitTile == 0 && this._isGameOver == false) {
                //this.addScore(this._hitTile);
                this.addScore();
            }
            else {
                //ゲームオーバー効果音を再生
                if (this._isGameOver == false) {
                    cc.audioEngine.play(this.gOverAudio,false);
                }
                this._isGameOver = true;
            }
        }
    },

    //ボタン1
    clickButton1: function () {
        if (this._isGameStart == true) {
            if (this._hitTile == 1 && this._isGameOver == false) {
                //this.addScore(this._hitTile);
                this.addScore();
            }
            else {
                //ゲームオーバー効果音を再生
                if (this._isGameOver == false) {
                    cc.audioEngine.play(this.gOverAudio,false);
                }
                this._isGameOver = true;
            }
        }
    },

    //ボタン2
    clickButton2: function () {
        if (this._isGameStart == true) {
            if (this._hitTile == 2 && this._isGameOver == false) {
                //this.addScore(this._hitTile);
                this.addScore();
            }
            else {
                //ゲームオーバー効果音を再生
                if (this._isGameOver == false) {
                    cc.audioEngine.play(this.gOverAudio,false);
                }
                this._isGameOver = true;
            }
        }
    },

    //ボタン3
    clickButton3: function () {
        if (this._isGameStart == true) {
            if (this._hitTile == 3 && this._isGameOver == false) {
                //this.addScore(this._hitTile);
                this.addScore();
            }
            else {
                //ゲームオーバー効果音を再生
                if (this._isGameOver == false) {
                    cc.audioEngine.play(this.gOverAudio,false);
                }
                this._isGameOver = true;
            }
        }
    },

    //スコアの加算とその他の処理
    addScore: function () {
        //ボタンをクリックしたら効果音を再生する
        cc.audioEngine.play(this.clickButtonAudio);
        //スコア加算
        this._score = (this._score + 1)|0;
        this.scoreManager.addScore();
        //子ノードを削除
        this.vPosChange.childDelete(this._arrayPointer);
        //座標の入れ替え
        this.vPosChange.posChange();
        //配列の要素を書き換え
        this.activeNum0[this._arrayPointer] = this.activeNum1[this._arrayPointer];
        //タイルを生成
        var sidetile = 0;
        while (sidetile < 4) {
            this.createTile(this._arrayPointer, sidetile, this.activeNum0[this._arrayPointer]);
            sidetile = (sidetile + 1)|0;
        }
        //ポインタを1進める
        this._arrayPointer = (this._arrayPointer + 1)|0;
        //ポインタが配列の最後まで参照した
        if (this._arrayPointer == 4) {
            console.log('_arrayPointre is Reset')
            //ポインタをリセット
            this._arrayPointer = 0;
            //配列の最初の数値をセット
            this._hitTile = this.activeNum0[this._arrayPointer];
            //再度activeNum1に値をセット
            this.setArray(1);
        }
        else {
        //_hitTileの数値を変更
        this._hitTile = this.activeNum0[this._arrayPointer];
        }
    },

    //画面の切り替え
    goResult: function () {
        //ゲーム画面を非表示に
        this.gameManager.active = false;
        //リザルト画面を表示する
        this.resultManager.active = true;
    },

    //動画を再生後スコアを引き継ぎリセット
    reSet: function () {
        //フラグや変数の値を戻す
        this._isGameStart = false;
        this._time = 10;
        this._isGameOver = false;
        this._isViewing = true;
        this._score = 0;
        this._frizztime = 2;

        //Canvasの状態を戻す
        this.startButton.active = true;
        this.gStartLabel.active = true;
        this.gOverLabel.active = false;
        this.timeGage.progress = this._time;

        //確認画面を非アクティブにする
        this.videoScrrn.active = false;

        //抽選関数を呼び出して乱数をセット(0,1)のどれか1つ
        //this._dice = this.random(0,1,1);
        
        this._isViewing2 = true;
        this.scoreManager.setWatchTrue();

        //音再生
        this._audioID = cc.audioEngine.play(this.gameBGM, true);
    },

    //広告の読み込み
    adPreload: function () {
        var preloadedRewardedVideo = null;
        try {
        FBInstant.getRewardedVideoAsync('591320481299550', //広告のID
        ).then(function(rewarded) {
            //非同期で広告をロード
            preloadedRewardedVideo = rewarded;
            return preloadedRewardedVideo.loadAsync();
        }).then(function() {
            console.log('動画を読み込み')
        }).catch(function(err){
            console.error('動画の読み込みに失敗しました：' + err.message);
        });
        } catch(error) {
            //console.log(error);
        }
    },

    //広告動画再生
    adShow: function() {
        try {
            //正常に動画を読み込んでいると再生する
            preloadedRewardedVideo.showAsync().then(function() {
                console.log('動画を再生');        
            }).catch(function(e) {
                  console.error(e.message);
            });
        } catch(error) {
            console.log('動画を再生出来ませんでした');
        }
        //リセット関数呼び出し
        this.reSet();
    }
});
