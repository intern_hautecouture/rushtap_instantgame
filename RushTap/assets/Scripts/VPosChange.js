// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        vartical0: {
            default: null,
            type: cc.Node
        },

        vartical1: {
            default: null,
            type: cc.Node
        },

        vartical2: {
            default: null,
            type: cc.Node
        },

        vartical3: {
            default: null,
            type: cc.Node
        },

        _v0pos: 0,
        _v1pos: 0,
        _v2pos: 0,
        _v3pos: 0,

        _loopLimit: 0
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        //getPosition()で座標を取得。
        this._v0pos = this.vartical0.getPosition();
        this._v1pos = this.vartical1.getPosition();
        this._v2pos = this.vartical2.getPosition();
        this._v3pos = this.vartical3.getPosition();
    },

    // update (dt) {},

    //座標の入れ替え
    posChange: function () {
        //1つ変数を作って座標の値を入れ替える。
        var self = 0;
        self = this._v0pos;
        this._v0pos = this._v3pos;
        this._v3pos = this._v2pos;
        this._v2pos = this._v1pos;
        this._v1pos = self;

        //setPosition()で入れ替え後の座標を設定
        this.vartical0.setPosition(this._v0pos);
        this.vartical1.setPosition(this._v1pos);
        this.vartical2.setPosition(this._v2pos);
        this.vartical3.setPosition(this._v3pos);
    },

    //子ノードを削除
    childDelete: function (num) {
        switch(num) {
            case 0:
            //変数に子ノードを入れる
            var children = this.vartical0.children;
            //while文で1つずつ子ノードを削除
            while (this._loopLimit < 4) {
                //子ノードを削除
                children[this._loopLimit].destroy();
                //1ずつ加算。インクリメント処理より圧倒的に早い。|0で型認識させる。
                this._loopLimit = (this._loopLimit + 1)|0;
            }
            break;

            case 1:
            var children = this.vartical1.children;
            while (this._loopLimit < 4) {
                children[this._loopLimit].destroy();
                this._loopLimit = (this._loopLimit + 1)|0;
            }
            break;

            case 2:
            var children = this.vartical2.children;
            while (this._loopLimit < 4) {
                children[this._loopLimit].destroy();
                this._loopLimit = (this._loopLimit + 1)|0;
            }
            break;

            case 3:
            var children = this.vartical3.children;
            while (this._loopLimit < 4) {
                children[this._loopLimit].destroy();
                this._loopLimit = (this._loopLimit + 1)|0;
            }
            break;
        }
        //使いまわすので最後に0に戻す
        this._loopLimit = 0;
    }
});
