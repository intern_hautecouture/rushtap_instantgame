// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

//変数宣言
//Gameを使用
var Game = require('Game');
//ScoreManagerを使用
var ScoreManager = require('ScoreManager');

cc.Class({
    extends: cc.Component,

    properties: {
        //ScoreManager
        scoreManager: {
            default: null,
            type: ScoreManager
        },

        //Game
        game: {
            default: null,
            type: Game
        },
    },

    //リトライボタンをタッチで実行
    goTitle: function () {
        //シーンを読み込む前にデータを記録
        this.scoreManager.writeUserData();
        //タイトルシーンを読み込で最初に戻る
        cc.director.loadScene('GameScene');
    }
});
