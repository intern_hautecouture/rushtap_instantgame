// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        //タイトルマネージャー
        titleManager: {
            default: null,
            type: cc.Node
        },
        //ゲームマネージャー
        gameManeger: {
            default: null,
            type: cc.Node
        }
    },

    //プレイボタンをタッチで実行
    gamePlay: function () {
        console.log('isPlay');
        //タイトル画面を非表示に
        this.titleManager.active = false;
        //ゲーム画面を表示する
        this.gameManeger.active = true;
    }
});
